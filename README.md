## Membres du groupe
- BURY Thomas
- CELLIER Titouan
- JEANNE Joseph

## Langage source du compilateur
.meme

## Langage cible du compilateur
C

## Langage du compilateur
C et CPP

## Description des fonctionnalités
Analyseur lexical

## Comment lancer le projet
- Sur votre invite de commande, taper `gcc analyseur.cpp -o test.exe`
- Exécutez `test.exe`
