#define _CRT_SECURE_NO_DEPRECATE
#include<stdio.h>
#include<stdlib.h>

int main() {
	const char *path = "source.meme";
	FILE *file = fopen(path, "r");
	if (file == NULL) {
		printf("Erreur d'ouverture du fichier source..\n\n");
		exit(0);
	}
	fclose(file);
	system("pause");
	return 0;
};